This repo has a single HTML file.  It is a TiddlyWiki file with a slowly increasing number of Pathfinder 2e's rules.  

From www.tiddlywiki.com, "TiddlyWiki is a rich, interactive tool for manipulating complex data with structure that doesn't easily fit into conventional tools like spreadsheets or wordprocessors."  It's essentially a non-linear notebook that has built-in tools and can include HTML, CSS, and javascript.  The file in this reop can be opened by any web browser or by dedicated applications like TiddlyDesktop.

This file is one that I started compiling for myself as I learn to be a PF2e GM.  I use TiddlyWiki for note-taking during sessions and I find it useful to have the rules easily referenceable in the same "notebook".
